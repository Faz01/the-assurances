﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheAssurances.Models;

namespace TheAssurances.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Registration()
        {
            return View(new registration());
        }
        [HttpPost]
        public ActionResult Registration(registration Models)
        {
                if(ModelState.IsValid)
            {
                var db = new Connections();
                db.users.Add(Models);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Models);
        }
    }
}