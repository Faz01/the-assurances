﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TheAssurances.Models
{
    public class Connections : DbContext
    {
        public Connections() : base("Connections")
        {

        }
        
        public DbSet<registration> users { get; set; }
    }
}