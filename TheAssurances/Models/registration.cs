﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;


namespace TheAssurances.Models
{
    public class registration 
    {
        
        public int id { get; set; }
        [Required]
        public int nik { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public string address { get; set; }
        [Required]
        public int phone_number { get; set; }
        [Required]
        public string user_id { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string email_id { get; set; }
        [Required]
        public string bank_id { get; set; }
        [Required]
        public int account_id { get; set; }
        [Required]
        public string bank_name { get; set; }

        public string getHashPass()
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            registration pass = new registration();

            Byte[] originalBytes = System.Text.ASCIIEncoding.Default.GetBytes(pass.password);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);

            string hash = BitConverter.ToString(encodedBytes).Replace("-", "").ToLower();
            return hash;
        }
        public class registrationInfoSet
        {
            public List<registration> registrationList { get; set; }

            public registrationInfoSet(params registration[] registrations)
            {
                registrationList = new List<registration>();
                foreach(var registration in registrations)
                {
                    registrationList.Add(registration);
                }
            }
        }
    }
}